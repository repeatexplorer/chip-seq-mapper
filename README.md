# ChIP-Seq-mapper #


Analysis of NGS sequences from Chromatin Imunoprecipitation. ChiP and Input reads are mapped to contigs obtained from graph based repetitive sequence clustering to enriched repeats. This method was used in (Neumann et al. 2012). for identification of repetitive sequences associated with cetromeric region.

## Authors ##
Petr Novak, Jiri Macas, Pavel Neumann, Georg Hermanutz

Biology Centre CAS, Czech Republic


## Installation and dependencies #

ChIP-Seq-mapper require NCBI blast to be installed, R programming language with installed R2HTML and base64 packages and python3

# Usage #

```
ChipSeqRatioAnalysis.py [-h] [-m MAX_CL] [-n NPROC] -c CHIPSEQ -i
                               INPUTSEQ [-o OUTPUT] [-ht HTML] [-t THRESHOLD]
                               -k CONTIGS

optional arguments:
  -h, --help            show this help message and exit
  -m MAX_CL, --max_cl MAX_CL
                        Sets the maximum cluster number. Default = 200
  -n NPROC, --nproc NPROC
                        Sets the number of cpus to be used. Default = all
                        available
  -c CHIPSEQ, --ChipSeq CHIPSEQ
                        Fasta file containing the Chip Sequences
  -i INPUTSEQ, --InputSeq INPUTSEQ
                        Fasta file containing the Input Sequences
  -o OUTPUT, --output OUTPUT
                        Specify a name for the CSV file to which the output
                        will be save to. Default: ChipSeqRatio.csv                      
  -ht HTML, --html HTML                                                                 
                        Specify a name for the html report. Default :                   
                        ChipSeqRatioReport                                              
  -t THRESHOLD, --threshold THRESHOLD                                                   
                        Optional plot filter. Default: mean ration between              
                        Input hits and Chip hits.                                       
  -k CONTIGS, --Contigs CONTIGS                                                         
                        Contig file for blast 
```

# References #
[PLoS Genet. Epub 2012 Jun 21. Stretching the rules: monocentric chromosomes with multiple centromere domains. Neumann P, Navrátilová A, Schroeder-Reiter E, Koblížková A, Steinbauerová V, Chocholová E, Novák P, Wanner G, Macas J.](http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1002777)
